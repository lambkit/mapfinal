package com.mapfinal.example.sql;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import com.mapfinal.dispatcher.sql.SingleSqlParserFactory;

public class SqlParserTest {
	public static void main(String arg[]) {
		System.out.println("欢迎您登录登录，请开始使用。");
		/*
		 * 应该有一个验证管理员和用户的功能。
		 */
		System.out.println("请出入sql语句");
		boolean temp = true;
		while (temp) {
			@SuppressWarnings("resource")
			Scanner input = new Scanner(System.in);
			String sql = input.nextLine();
			/*
			 * 预处理:获得语句; 处理前后空格; 变小写; 处理中间多余的空格回车和特殊字符; 在末尾加特殊符号; 处理掉最后的;
			 */

			// 处理分行输入的问题，就是读;号才停止;
			while (sql.lastIndexOf(";") != sql.length() - 1) {
				sql = sql + " " + input.nextLine();
			}

			sql = sql.trim();
			sql = sql.toLowerCase();
			sql = sql.replaceAll("\\s+", " ");

			sql = sql.substring(0, sql.lastIndexOf(";"));
			sql = "" + sql + " ENDOFSQL";
			System.out.println(sql);

			/*
			 * 结束输入判断
			 */
//			Pattern pattern=Pattern.compile("(quit)");
//			Matcher matcher=pattern.matcher(sql);
//			System.out.println(matcher.find());
			@SuppressWarnings("unused")
			List<List<String>> parameter_list = new ArrayList<List<String>>();

			if (sql.equals("quit")) {
				temp = false;
			} else {
				// System.out.println("准备执行SingleSqlParserFactory.generateParser函数");
				/*
				 * 工厂类判断调用什么语句的正则表达分割
				 */
				parameter_list = SingleSqlParserFactory.generateParser(sql);
//----------------------------------------------------------------------------------------------------------------
				System.out.println("执行结束SingleSqlParserFactory.generateParser函数");

				for (int i = 0; i < parameter_list.size(); i++) {
					System.out.println(parameter_list.get(i));
				}

//				System.out.println();
//				
//				List<String> test = new ArrayList<String>();
//				test = parameter_list.get(2);
//				
//				for(int i = 0;i < test.size();i++)
//				{
//					String r = test.get(i);
//					System.out.println(r.trim());
//				}
//----------------------------------------------------------------------------------------------------------------
			}

		}

	}
}
