package com.mapfinal.example.style;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.mapfinal.render.style.SimpleFillSymbol;

public class StyleJsonTest {

	public static void main(String[] args) {
		SimpleFillSymbol symbol = SimpleFillSymbol.DEFAULT();
		JSONObject json = symbol.toJson();
		System.out.println(json.toJSONString());
		
		String jsonSymbol = json.toString();
		SimpleFillSymbol newSymbol = new SimpleFillSymbol(0);
		newSymbol.fromJson(JSON.parseObject(jsonSymbol));
		System.out.println(newSymbol.getOpacity());
		System.out.println(newSymbol.getColor());
		System.out.println(newSymbol.getType());
		System.out.println(newSymbol.getOutline().getColor());
		System.out.println(newSymbol.getOutline().getWidth());
		System.out.println("---------------over----------------");	
	}
}
