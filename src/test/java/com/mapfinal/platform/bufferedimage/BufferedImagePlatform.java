package com.mapfinal.platform.bufferedimage;


import java.awt.image.BufferedImage;

import com.mapfinal.Platform;
import com.mapfinal.geometry.FloatPackedCS;
import com.mapfinal.geometry.GeoKit;
import com.mapfinal.geometry.MapCSFactory;
import com.mapfinal.resource.image.ImageHandle;

public class BufferedImagePlatform implements Platform {

	private BufferedImageHandle imageHandle;
	
	@Override
	public void init() {
		// TODO Auto-generated method stub
	}
	
	@Override
	public void initGeometryFactory() {
		// TODO Auto-generated method stub
		//GeoKit.initGeometryFactory(new MapCSFactory(ListMapCS.class));
		GeoKit.initGeometryFactory(new MapCSFactory(FloatPackedCS.class));
	}
	

	@Override
	public ImageHandle<BufferedImage> getImageHandle() {
		// TODO Auto-generated method stub
		if(imageHandle==null) {
			imageHandle = new BufferedImageHandle();
		}
		return imageHandle;
	}

}
