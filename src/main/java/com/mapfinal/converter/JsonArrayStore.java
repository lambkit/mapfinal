package com.mapfinal.converter;

import com.alibaba.fastjson.JSONArray;

public interface JsonArrayStore {

	void fromJson(JSONArray jsonArray);
	
	JSONArray toJson();
}
