package com.mapfinal.converter;

import org.dom4j.Document;

public interface XmlStore {
	
	void fromXml(Document jsonObject);

	Document toXml();
}
