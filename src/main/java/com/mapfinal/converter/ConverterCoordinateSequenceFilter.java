package com.mapfinal.converter;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.CoordinateSequence;
import org.locationtech.jts.geom.CoordinateSequenceFilter;

public class ConverterCoordinateSequenceFilter implements CoordinateSequenceFilter {
	
	private Converter converter;
	
	public ConverterCoordinateSequenceFilter(Converter converter) {
		// TODO Auto-generated constructor stub
		this.converter = converter;
	}

//	@Override
//	public void filter(Coordinate coord) {
//		// TODO Auto-generated method stub
//		if(converter!=null) {
//			System.out.println("transform coordinate: " + coord);
//			coord = converter.transform(coord);
//			System.out.println("transform coordinate: " + coord);
//		}
//	}

	@Override
    public void filter(CoordinateSequence seq, int i) {
        //System.out.println("transform coordinate0: " + seq.getCoordinate(i));
        Coordinate coord = converter.transform(seq.getCoordinate(i));
        seq.setOrdinate(i, CoordinateSequence.X, coord.x);
        seq.setOrdinate(i, CoordinateSequence.Y, coord.y);
        //System.out.println("transform coordinate1: " + seq.getCoordinate(i));
    }

    @Override
    public boolean isGeometryChanged() {
        return true;
    }

    @Override
    public boolean isDone() {
        return false;
    }

}
