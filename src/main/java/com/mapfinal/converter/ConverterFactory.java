package com.mapfinal.converter;

import com.mapfinal.converter.scene.SceneCRS;

public interface ConverterFactory {
	public String getType();
	public CRS getParameters(String crsName);
	public Converter build(String sourceCRS, String targetCRS);
	public Converter build(CRS sourceCRS, CRS targetCRS);
	
	public CRS parse(String name, String code);
	public CRS parseWKT(String name, String wkt);
	
	public SceneCRS toSceneCRS(CRS crs);
}
