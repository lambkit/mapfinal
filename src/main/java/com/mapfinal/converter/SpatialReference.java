package com.mapfinal.converter;

public class SpatialReference {
	
	private String name;
	
	public SpatialReference(String name) {
		// TODO Auto-generated constructor stub
		this.name = name;
	}
	
	public static SpatialReference mercator() {
		return new SpatialReference("EPSG:3857");
	}
	
	public static SpatialReference wgs84() {
		return new SpatialReference("EPSG:4326");
	}

	public static SpatialReference create(int wkid) {
		//未完成
		return new SpatialReference("EPSG:4326");
	}

	public static SpatialReference create(java.lang.String wktext) {
		//未完成
		return new SpatialReference("EPSG:4326");

	}

	public java.lang.String getText() {
		return null;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		if(obj instanceof SpatialReference) {
			SpatialReference srs = (SpatialReference) obj;
			if(getName()==null) {
				if(srs.getName()==null) {
					return true;
				} else {
					return false;
				}
			}
			return getName().equals(srs.getName());
		}
		return super.equals(obj);
	}
	// public boolean equals(java.lang.Object o) {}
}
