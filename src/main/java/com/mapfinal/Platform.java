package com.mapfinal;

import com.mapfinal.resource.image.ImageHandle;

public interface Platform {

	/**
	 * 初始化
	 */
	public void init();
	
	/**
	 * 初始化GeometryFactory, 仅系统启动时调用一次
	 */
	public void initGeometryFactory();
	
	/**
	 * 创建一个图像处理句柄
	 * @return
	 */
	public ImageHandle getImageHandle();
	
}
