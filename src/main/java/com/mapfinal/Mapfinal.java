package com.mapfinal;

import java.io.File;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import com.mapfinal.event.EventKit;
import com.mapfinal.kit.StringKit;
import com.mapfinal.map.MapScene;
import com.mapfinal.resource.image.ImageHandle;

/**
 * 参考： 天地图Android-SDK API
 * http://www.njmap.gov.cn/geomap-api/apidoc/androidJavaApiDoc/index.html?overview-summary.html
 * ArcGIS Runtime SDK for Java 100.7.0
 * https://developers.arcgis.com/java/latest/api-reference/reference/index.html
 * 
 * @author yangyong
 *
 */
public class Mapfinal {

	private static final String DEFAULT = "main";
	private static final Mapfinal me = new Mapfinal();

	private Map<String, MapScene> mapScenes;
	private Platform platform;
	/**
	 * 系统缓存地址
	 */
	private String cacheFolder;
	private int pickPointZommPixel = 5;
	private int pickLineZommPixel = 10;

	private Mapfinal() {
		mapScenes = new ConcurrentHashMap<String, MapScene>();
		cacheFolder = System.getProperty("user.dir") + File.separator + "cache";
	}

	public static Mapfinal me() {
		return me;
	}

	public static MapScene use() {
		return me.mapScenes.get(DEFAULT);
	}

	public static MapScene use(String name) {
		return me.mapScenes.get(name);
	}

	public void init(Platform platform, MapScene mapScene) {
		this.platform = platform;
		this.platform.init();
		this.platform.initGeometryFactory();

		mapScenes.put(DEFAULT, mapScene);
	}

	public Set<String> mapSceneNames() {
		return mapScenes.keySet();
	}

	public int mapSceneSize() {
		return mapScenes.size();
	}

	public void addMapScene(String name, MapScene mapScene) {
		name = name == null ? DEFAULT : name;
		mapScenes.put(name, mapScene);
	}

	/**
	 * 设置重绘
	 * 
	 * @param b
	 */
	public void setRedraw(String name, boolean b) {
		if (StringKit.isBlank(name)) {
			for (Entry<String, MapScene> scene : mapScenes.entrySet()) {
				scene.getValue().setRedraw(b);
			}
		} else {
			MapScene scene = mapScenes.get(name);
			if (scene != null) {
				scene.setRedraw(b);
			}
		}
	}

	/**
	 * 重绘
	 */
	public static void redraw(String name) {
		EventKit.sendEvent("redraw", "name", name);
	}

	public Platform platform() {
		return platform;
	}

	public ImageHandle imageHandle() {
		return getPlatform().getImageHandle();
	}

	/**
	 * 点拾取的时候，放大多少像素
	 * 
	 * @return
	 */
	public int getPickPointZommPixel() {
		return pickPointZommPixel;
	}

	/**
	 * 线拾取的时候，放大多少像素
	 * 
	 * @return
	 */
	public int getPickLineZommPixel() {
		return pickLineZommPixel;
	}

	public void setPickPointZommPixel(int pickPointZommPixel) {
		this.pickPointZommPixel = pickPointZommPixel;
	}

	public void setPickLineZommPixel(int pickLineZommPixel) {
		this.pickLineZommPixel = pickLineZommPixel;
	}

	public String getCacheFolder() {
		return cacheFolder;
	}

	public void setCacheFolder(String cacheFolder) {
		this.cacheFolder = cacheFolder;
	}

	public Platform getPlatform() {
		return platform;
	}

	public void setPlatform(Platform platform) {
		this.platform = platform;
	}
}
