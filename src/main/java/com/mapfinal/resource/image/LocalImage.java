package com.mapfinal.resource.image;

import com.mapfinal.event.Event;

public class LocalImage<M> extends Image<M> {

	public LocalImage(String name, String url) {
		super(name, url, null);
		setFileType(FileType.file);
	}

	@Override
	public LocalImage<M> read(Event event) {
		// TODO Auto-generated method stub
		if(this.data==null) {
			this.data = (M) getHandle().readFile(getUrl());
		}
		if(this.data == null) {
			System.out.println("read local image is null");
		} else {
			System.out.println("read local image");
		}
		return this;
	}
}
