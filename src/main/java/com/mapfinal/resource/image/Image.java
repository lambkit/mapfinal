package com.mapfinal.resource.image;

import com.alibaba.fastjson.JSONObject;
import com.mapfinal.Mapfinal;
import com.mapfinal.event.Event;
import com.mapfinal.render.style.MarkerSymbol;
import com.mapfinal.resource.Data;
import com.mapfinal.resource.Resource;
import com.mapfinal.resource.ResourceObject;

public class Image<M> extends ResourceObject<Image<M>> implements Data {
	/**
	 * 存储类型
	 */
	private FileType fileType = FileType.cache;
	/**
	 * 图像类型
	 */
	private Resource.ImageType imageType = ImageType.png;
	/**
	 * 图像数据
	 */
	protected M data;
	
	public Image(String name, String url, M image) {
		this.setName(name);
		this.setUrl(url);
		this.setType(Resource.Type.image.name());
		this.data = image;
	}
	
	public ImageHandle<M> getHandle() {
		return (ImageHandle<M>) Mapfinal.me().platform().getImageHandle();
	}
	
	@Override
	public void prepare(Event event) {
		// TODO Auto-generated method stub
	}

	@Override
	public Image<M> read(Event event) {
		// TODO Auto-generated method stub
		return this;
	}

	@Override
	public void writer(Event event, Image<M> image) {
		// TODO Auto-generated method stub
		if(image==null) {
			getHandle().writeFile(image.getUrl(), image.getData(event));
		}
	}
	
	public void writer(Event event) {
		if(this.data==null) {
			getHandle().writeFile(getUrl(), getData(event));
		}
	}
	
	/**
	 * 获取并读取图像
	 * @return
	 */
	public M getData(Event event) {
		read(event);
		return data;
	}
	
	public int getWidth() {
		return data==null ? 0 : getHandle().getWidth(data);
	}

	public int getHeight() {
		return data==null ? 0 : getHandle().getHeight(data);
	}
	
	

	public void setData(M image) {
		this.data = image;
	}

	public Resource.ImageType getImageType() {
		return imageType;
	}

	public void setImageType(Resource.ImageType imageType) {
		this.imageType = imageType;
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		data = null;
	}

	public FileType getFileType() {
		return fileType;
	}

	public void setFileType(FileType fileType) {
		this.fileType = fileType;
	}

	public void fromJson(JSONObject jsonObject) {
		// TODO Auto-generated method stub
		super.fromJson(jsonObject);
		fileType = jsonObject.getObject("fileType", FileType.class);
		imageType = jsonObject.getObject("imageType", Resource.ImageType.class);
		if(jsonObject.containsKey("data")) {
			String imageString = jsonObject.getString("data");
			data = (M) Mapfinal.me().imageHandle().decodeToImage(imageString);
		}
	}

	public JSONObject toJson() {
		// TODO Auto-generated method stub
		JSONObject json = super.toJson();
		json.put("fileType", fileType);
		json.put("imageType", imageType);
		if(data!=null) {
			json.put("data", Mapfinal.me().imageHandle().encodeToString(data, imageType.name()));
		}
		return json;
	}
	
}
