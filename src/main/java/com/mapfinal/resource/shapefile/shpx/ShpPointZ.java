package com.mapfinal.resource.shapefile.shpx;

public class ShpPointZ {
	/**
	 * 字节数
	 */
	public static final int SIZE = 32;
	public double x;
	public double y;
	public double z;
	public double m;
}
