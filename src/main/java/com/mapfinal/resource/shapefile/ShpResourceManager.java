package com.mapfinal.resource.shapefile;

import java.util.Map;

import com.mapfinal.common.SyncWriteMap;

public class ShpResourceManager {

	private ShapefileReaderFactory readerFactory;
	private Map<String, ShpResource> resources;
	
	public ShpResourceManager() {
		this.setReaderFactory(new DefaultShapefileReaderFactory());
		resources = new SyncWriteMap<String, ShpResource>(32, 0.25F);
	}

	private static final ShpResourceManager me = new ShpResourceManager();
	
	public static ShpResourceManager me() {
		return me;
	}
	
	public ShpResource create(String url) {
		ShpResource shp = getResource(url);
		if(shp==null) {
			shp = new ShpResource(url);
			addResource(url, shp);
		}
		return shp;
	}
	
	public ShpResource create(String url, String charsetName) {
		ShpResource shp = getResource(url);
		if(shp==null) {
			shp = new ShpResource(url, charsetName);
			addResource(url, shp);
		}
		return shp;
	}
	
	public String getResourceType() {
		return "shp";
	}
	
	public long getMemorySize() {
		long memorySize = 0;
		for (ShpResource tc : resources.values()) {
			memorySize += tc.getMemorySize();
		}
		return memorySize;
	}

	public void addResource(String key, ShpResource resources) {
		this.resources.put(key, resources);
	}

	public ShpResource getResource(String key) {
		return resources.get(key);
	}
	
	public void remove(String key) {
		this.resources.remove(key);
	}

	public void clear() {
		this.resources.clear();
	}
	
	public ShapefileReaderFactory getReaderFactory() {
		return readerFactory;
	}

	public void setReaderFactory(ShapefileReaderFactory readerFactory) {
		this.readerFactory = readerFactory;
	}
}
