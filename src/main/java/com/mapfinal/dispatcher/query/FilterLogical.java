/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mapfinal.dispatcher.query;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class FilterLogical implements Filter, Serializable {

	private static final long serialVersionUID = 1L;
	protected String op;//operator; and, or
	protected List<Filter> expressionList;
	
	public FilterLogical(String op) {
		// TODO Auto-generated constructor stub
		expressionList = new ArrayList<Filter>();
		this.op = op;
	}
	
	public FilterLogical(String op, List<Filter> expressions) {
		// TODO Auto-generated constructor stub
		expressionList = expressions==null ? new ArrayList<Filter>() : expressions;
		this.op = op;
	}
	
	public static FilterLogical and() {
		return new FilterLogical("and");
	}
	
	public static FilterLogical or() {
		return new FilterLogical("or");
	}
	
	public static FilterLogical not() {
		return new FilterLogical("not");
	}
	
	public static FilterLogical and(List<Filter> expressions) {
		return new FilterLogical("and", expressions);
	}
	
	public static FilterLogical or(List<Filter> expressions) {
		return new FilterLogical("or", expressions);
	}
	
	public static FilterLogical not(List<Filter> expressions) {
		return new FilterLogical("not", expressions);
	}
	
	public FilterLogical add(Filter expression) {
		expressionList.add(expression);
		return this;
	}
	
	public int size() {
		return expressionList.size();
	}
	
	public String getOp() {
		return op;
	}

	public void setOp(String op) {
		this.op = op;
	}

	@Override
	public SqlParas getSqlParas(Criteria criteria) {
		// TODO Auto-generated method stub
		SqlParas csql = new SqlParas();
		for (Filter expression : expressionList) {
			SqlParas esql = expression.getSqlParas(criteria);
			if(op.equalsIgnoreCase("and")) {
				csql = csql.and(criteria, expression);
			} else if(op.equalsIgnoreCase("or")) {
				csql = csql.or(criteria, expression);
			}
		}
		return csql;
	}

	@Override
	public boolean evaluate(Object object) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void accept(FilterVisitor visitor, Object extraData) {
		// TODO Auto-generated method stub
		
	}

}
