package com.mapfinal.dispatcher.query;

public enum ExpressionType {
	ATTRIBUTE,//String
	ATTRIBUTE_DOUBLE,
	ATTRIBUTE_GEOMETRY,
	ATTRIBUTE_INTEGER,
	ATTRIBUTE_STRING,
	ATTRIBUTE_UNDECLARED,
	FUNCTION,//Defines a function expression
	LITERAL_DOUBLE,
	LITERAL_GEOMETRY,//Defines a literal expression with a declared geometry type.
	LITERAL_INTEGER,
	LITERAL_LONG,
	LITERAL_STRING,
	LITERAL_UNDECLARED,
	MATH_ADD,
	MATH_DIVIDE,
	MATH_MULTIPLY,
	MATH_SUBTRACT
}
