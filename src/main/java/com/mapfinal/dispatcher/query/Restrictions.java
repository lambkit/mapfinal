/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mapfinal.dispatcher.query;

import java.util.List;

public class Restrictions {

	public static Criteria createCriteria(String tableName) {
		Criteria criteria = new Criteria(tableName);
		return criteria;
	}
	
	public static Criteria createCriteria(String tableName, FetchMode fetchMode) {
		Criteria criteria = new Criteria(tableName);
		criteria.setFetchMode(fetchMode);
		if(fetchMode!=FetchMode.MAIN) criteria.setAlias(tableName);
		return criteria;
	}
	
	public static Criteria createCriteria(String tableName, String alias, FetchMode fetchMode) {
		Criteria criteria = new Criteria(tableName, alias);
		return criteria;
	}
	
	public static FilterLogical and() {
		return new FilterLogical("and");
	}

	public static FilterLogical and(List<Filter> expressions) {
		return new FilterLogical("and", expressions);
	}

	public static FilterLogical or() {
		return new FilterLogical("or");
	}
	
	public static FilterLogical or(List<Filter> expressions) {
		return new FilterLogical("or", expressions);
	}
	
	public static FilterLogical not() {
		return new FilterLogical("not");
	}
	
	public static FilterLogical not(List<Filter> expressions) {
		return new FilterLogical("not", expressions);
	}

	public static Filter between(String propertyName, Object lo, Object hi) {
		return new ExpressionCondition(ConditionMode.BETWEEN, propertyName, lo, hi);
	}

	public static Filter eq(String propertyName, Object value) {
		return new ExpressionCondition(ConditionMode.EQUAL, propertyName, value);
	}

	public static ExpressionProperty eqProperty(String propertyName, String otherPropertyName) {
		return new ExpressionProperty(propertyName, otherPropertyName, ConditionMode.EQUAL);
	}

	public static Filter ge(String propertyName, Object value) {
		return new ExpressionCondition(ConditionMode.GREATER_EQUAL, propertyName, value);
	}

	public static ExpressionProperty geProperty(String propertyName, String otherPropertyName) {
		return new ExpressionProperty(propertyName, otherPropertyName, ConditionMode.GREATER_EQUAL);
	}

	public static Filter gt(String propertyName, Object value) {
		return new ExpressionCondition(ConditionMode.GREATER_THEN, propertyName, value);
	}

	public static ExpressionProperty gtProperty(String propertyName, String otherPropertyName) {
		return new ExpressionProperty(propertyName, otherPropertyName, ConditionMode.GREATER_THEN);
	}

	public static Filter ilike(String propertyName, Object value) {
		return new ExpressionCondition(ConditionMode.IFUZZY, propertyName, value);
	}
	
	public static Filter ilikeLeft(String propertyName, Object value) {
		return new ExpressionCondition(ConditionMode.IFUZZY_LEFT, propertyName, value);
	}
	
	public static Filter ilikeRight(String propertyName, Object value) {
		return new ExpressionCondition(ConditionMode.IFUZZY_RIGHT, propertyName, value);
	}
	
	public static Filter inotlike(String propertyName, Object value) {
		return new ExpressionCondition(ConditionMode.NOT_IFUZZY, propertyName, value);
	}
	
	public static Filter inotlikeLeft(String propertyName, Object value) {
		return new ExpressionCondition(ConditionMode.NOT_IFUZZY_LEFT, propertyName, value);
	}
	
	public static Filter inotlikeRight(String propertyName, Object value) {
		return new ExpressionCondition(ConditionMode.NOT_IFUZZY_RIGHT, propertyName, value);
	}

	public static Filter query(String propertyName, String value, ConditionMode type) {
		return new ExpressionCondition(type, propertyName, value);
	}

	public static Filter in(String propertyName, List<?> values) {
		return new ExpressionCondition(ConditionMode.IN, propertyName, values);
	}
	
	public static Filter in(String propertyName, Object[] values) {
		return new ExpressionCondition(ConditionMode.IN, propertyName, values);
	}

	public static Filter isEmpty(String propertyName) {
		return new ExpressionCondition(ConditionMode.EMPTY, propertyName);
	}

	public static Filter isNotEmpty(String propertyName) {
		return new ExpressionCondition(ConditionMode.NOT_EMPTY, propertyName);
	}

	public static Filter isNotNull(String propertyName) {
		return new ExpressionCondition(ConditionMode.NOT_NULL, propertyName);
	}

	public static Filter isNull(String propertyName) {
		return new ExpressionCondition(ConditionMode.ISNULL, propertyName);
	}

	public static Filter le(String propertyName, Object value) {
		return new ExpressionCondition(ConditionMode.LESS_EQUAL, propertyName, value);
	}

	public static ExpressionProperty leProperty(String propertyName, String otherPropertyName) {
		return new ExpressionProperty(propertyName, otherPropertyName, ConditionMode.LESS_EQUAL);
	}

	public static Filter like(String propertyName, Object value) {
		return new ExpressionCondition(ConditionMode.FUZZY, propertyName, value);
	}
	
	public static Filter notlike(String propertyName, Object value) {
		return new ExpressionCondition(ConditionMode.NOT_FUZZY, propertyName, value);
	}

	public static Filter lt(String propertyName, Object value) {
		return new ExpressionCondition(ConditionMode.LESS_THEN, propertyName, value);
	}

	public static ExpressionProperty ltProperty(String propertyName, String otherPropertyName) {
		return new ExpressionProperty(propertyName, otherPropertyName, ConditionMode.LESS_THEN);
	}

	public static Filter ne(String propertyName, Object value) {
		return new ExpressionCondition(ConditionMode.NOT_EMPTY, propertyName, value);
	}

	public static ExpressionProperty neProperty(String propertyName, String otherPropertyName) {
		return new ExpressionProperty(propertyName, otherPropertyName, ConditionMode.NOT_EQUAL);
	}

	public static Filter not(Filter expression) {
		return new ExpressionNot(expression);
	}

	public static Filter sql(String sql) {
		return new ExpressionSql(sql);
	}

	public static Filter sql(String sql, Object[] values) {
		return new ExpressionSql(sql, values);
	}

	public static Filter sql(String sql, Object value) {
		return new ExpressionSql(sql, value);
	}
}
