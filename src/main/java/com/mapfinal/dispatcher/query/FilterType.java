package com.mapfinal.dispatcher.query;

public enum FilterType {
	ALL,//Defines a sieve filter, with static implementation Filter.EXCLUDE
	NOT_NULL, // 不为null值的情况
	ISNULL, // null值的情况
	NOT_EMPTY, // 不为空值的情况
	EMPTY, // 空值的情况
	IN, // 在范围内
	NOT_IN, // 不在范围内
	BETWEEN, // 介于
	NOT_BETWEEN, // 非介于
	COMPARE_EQUALS,
	COMPARE_GREATER_THAN,
	COMPARE_GREATER_THAN_EQUAL,
	COMPARE_LESS_THAN,
	COMPARE_LESS_THAN_EQUAL,
	COMPARE_NOT_EQUALS,
	FID,
	GEOMETRY_BBOX,//Defines a geometric bounding box filter.
	GEOMETRY_BEYOND,//Defines a geometric 'BEYOND' operator.
	GEOMETRY_CONTAINS,// 几何空间对象A包含空间对象B
	GEOMETRY_CROSSES,// 交叉
	GEOMETRY_DISJOINT,// 相连
	GEOMETRY_DWITHIN,// 点缓冲区查询特定图层,查a点指定半径内的所有point类型记录
	GEOMETRY_EQUALS,// 相等
	GEOMETRY_INTERSECTS,// 相交
	GEOMETRY_OVERLAPS,// 两个几何空间数据存在交迭
	GEOMETRY_TOUCHES,// 接触
	GEOMETRY_WITHIN,// 几何空间对象A存在空间对象B中
	GEOMETRY_DISTANCE,//mapfinal-距离
	GEOMETRY_COVERS, // mapfinal-几何空间对象B中的所有点都在空间对象A中
	GEOMETRY_COVEREDBY, // mapfinal-几何空间对象A中的所有点都在空间对象B中
	LIKE,//Defines a like filter, which is implemented by FilterLike.
	NOT_LIKE,
	LOGIC_AND,//Defines a logical 'AND' filter.
	LOGIC_NOT,
	LOGIC_OR,
	NONE,
	NULL
}
