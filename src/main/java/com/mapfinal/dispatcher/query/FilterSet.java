package com.mapfinal.dispatcher.query;

import com.alibaba.fastjson.JSONObject;
import com.mapfinal.converter.JsonStore;

/**
 * 使用 Junction 设置查询条件。
 * 使用Projection 设置Group查询条件。
 * @author yangyong
 *
 */
public class FilterSet implements JsonStore {

	//查询条件
	private Filter filter;
	
	public FilterSet(Filter filter) {
		this.filter = filter;
	}
	
	public boolean evaluate(Object object) {
		return filter.evaluate(object);
	}
	
	public void accept(FilterVisitor visitor, Object extraData) {
		filter.accept(visitor, extraData);
	}

	@Override
	public void fromJson(JSONObject jsonObject) {
		// TODO Auto-generated method stub
	}

	@Override
	public JSONObject toJson() {
		// TODO Auto-generated method stub
		return null;
	}
}
