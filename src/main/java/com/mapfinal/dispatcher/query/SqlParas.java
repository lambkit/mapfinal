/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mapfinal.dispatcher.query;

import java.util.ArrayList;
import java.util.List;

import com.mapfinal.kit.StringKit;

public class SqlParas extends QueryParas {

	public SqlParas() {
	}
	
	public SqlParas(String sql) {
		// TODO Auto-generated constructor stub
		setSql(sql);
	}
	
	public SqlParas(String sql, List<Object> paraList) {
		// TODO Auto-generated constructor stub
		setSql(sql);
		setParaList(paraList);
	}
	
	public SqlParas(String sql, Object[] paraList) {
		// TODO Auto-generated constructor stub
		setSql(sql);
		setParaList(java.util.Arrays.asList(paraList));
	}
	
	public SqlParas(String sql, Object value) {
		// TODO Auto-generated constructor stub
		setSql(sql);
		setParaList(new ArrayList<>());
		add(value);
	}
	
	/********************************************************/
	
	public SqlParas and(Criteria criteria, Filter criterion) {
		if(criterion==null) return this;
		StringBuilder sb = new StringBuilder();
		if(StringKit.notBlank(getSql())) sb.append(getSql());
		SqlParas csql = criterion.getSqlParas(criteria);
		if(StringKit.notBlank(csql.getSql())) {
			if(criterion instanceof FilterLogical) {
				if(StringKit.notBlank(getSql())) sb.append(" and (");
				sb.append(csql.getSql());
				sb.append(") ");
			} else {
				if(StringKit.notBlank(getSql())) sb.append(" and ");
				sb.append(csql.getSql());
			}
			setSql(sb.toString());
			put(csql.getParas());
		}
		return this;
	}
	
	
	public SqlParas or(Criteria criteria, Filter criterion) {
		if(criterion==null) return this;
		StringBuilder sb = new StringBuilder();
		SqlParas csql = criterion.getSqlParas(criteria);
		if(StringKit.notBlank(csql.getSql())) {
			if(criterion instanceof FilterLogical) {
				if(StringKit.notBlank(getSql())) {
					sb.append(" (");
					sb.append(getSql());
					sb.append(")");
				}
				sb.append(" or (");
				sb.append(csql.getSql());
				sb.append(") ");
			} else {
				if(StringKit.notBlank(getSql())) {
					sb.append(getSql());
					sb.append(" or ");
				}
				sb.append(csql.getSql());
			}
			setSql(sb.toString());
			put(csql.getParas());
		}
		return this;
	}
	
	public SqlParas getSqlParas(Criteria criteria) {
		if(StringKit.notBlank(getSqlExceptSelect())) {
			if(StringKit.notBlank(criteria.getAlias())) {
				setSql(getSql().replace("{alias}", criteria.getAlias()));
			} else {
				setSql(getSql().replace("{alias}.", ""));
			}
		}
		return this;
	}
	
	public SqlParas selectAll(Criteria criteria) {
		int joinSize = criteria.joinSize();
		if(joinSize==0) {
			setSelect("select * ");
		} else {
			StringBuilder sb = new StringBuilder();
			sb.append("select").append(criteria.getAlias()).append(".*");
			for (Criteria crt : criteria.getCriteriaList()) {
				String _alias = crt.getAlias();
				_alias = StringKit.isBlank(_alias) ? crt.getTableName() : _alias;
				sb.append(", ").append(_alias).append(".*");
			}
			sb.append(" ");
			setSelect(sb.toString());
		}
		return this;
	}
	/********************************************************/
	
	public Integer getPageSize() {
		if(getParaList()==null) return 0;
		return getParaList().size();
	}
}
