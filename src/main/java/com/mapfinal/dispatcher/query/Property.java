/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mapfinal.dispatcher.query;

import java.util.List;

/**
 * Property实例是获得一个条件的另外一种途径。你可以通过调用Property.forName() 创建一个
Property。
Property age = Property.forName("age");
List cats = sess.createCriteria(Cat.class)
.add( Restrictions.disjunction()
.add( age.isNull() )
.add( age.eq( new Integer(0) ) )
.add( age.eq( new Integer(1) ) )
.add( age.eq( new Integer(2) ) )
) )
.add( Property.forName("name").in( new String[] { "Fritz", "Izi", "Pk" } ) )
.list();
 * @author 孤竹行
 *
 */
public class Property implements Expression {
	
	private String propertyName;
	private ExpressionType type;
	
	public Property(String name) {
		// TODO Auto-generated constructor stub
		this.propertyName = name;
	}
	
	public static Property forName(String name) {
		return new Property(name);
	}
	
	public Filter between(Object lo, Object hi) {
		return new ExpressionCondition(ConditionMode.BETWEEN, propertyName, lo, hi);
	}

	public Filter eq(Object value) {
		return new ExpressionCondition(ConditionMode.EQUAL, propertyName, value);
	}

	public ExpressionProperty eqProperty(String otherPropertyName) {
		return new ExpressionProperty(propertyName, otherPropertyName, ConditionMode.EQUAL);
	}

	public Filter ge(Object value) {
		return new ExpressionCondition(ConditionMode.GREATER_EQUAL, propertyName, value);
	}

	public ExpressionProperty geProperty(String otherPropertyName) {
		return new ExpressionProperty(propertyName, otherPropertyName, ConditionMode.GREATER_EQUAL);
	}

	public Filter gt(Object value) {
		return new ExpressionCondition(ConditionMode.GREATER_THEN, propertyName, value);
	}

	public ExpressionProperty gtProperty(String otherPropertyName) {
		return new ExpressionProperty(propertyName, otherPropertyName, ConditionMode.GREATER_THEN);
	}

	public Filter ilike(Object value) {
		return new ExpressionCondition(ConditionMode.IFUZZY, propertyName, value);
	}
	
	public Filter ilikeLeft(Object value) {
		return new ExpressionCondition(ConditionMode.IFUZZY_LEFT, propertyName, value);
	}
	
	public Filter ilikeRight(Object value) {
		return new ExpressionCondition(ConditionMode.IFUZZY_RIGHT, propertyName, value);
	}
	
	public Filter inotlike(Object value) {
		return new ExpressionCondition(ConditionMode.NOT_IFUZZY, propertyName, value);
	}
	
	public Filter inotlikeLeft(Object value) {
		return new ExpressionCondition(ConditionMode.NOT_IFUZZY_LEFT, propertyName, value);
	}
	
	public Filter inotlikeRight(Object value) {
		return new ExpressionCondition(ConditionMode.NOT_IFUZZY_RIGHT, propertyName, value);
	}

	public Filter query(String value, ConditionMode type) {
		return new ExpressionCondition(type, propertyName, value);
	}

	public Filter in(List<?> values) {
		return new ExpressionCondition(ConditionMode.IN, propertyName, values);
	}
	
	public Filter in(Object[] values) {
		return new ExpressionCondition(ConditionMode.IN, propertyName, values);
	}

	public Filter isEmpty() {
		return new ExpressionCondition(ConditionMode.EMPTY, propertyName);
	}

	public Filter isNotEmpty() {
		return new ExpressionCondition(ConditionMode.NOT_EMPTY, propertyName);
	}

	public Filter isNotNull() {
		return new ExpressionCondition(ConditionMode.NOT_NULL, propertyName);
	}

	public Filter isNull() {
		return new ExpressionCondition(ConditionMode.ISNULL, propertyName);
	}

	public Filter le(Object value) {
		return new ExpressionCondition(ConditionMode.LESS_EQUAL, propertyName, value);
	}

	public ExpressionProperty leProperty(String otherPropertyName) {
		return new ExpressionProperty(propertyName, otherPropertyName, ConditionMode.LESS_EQUAL);
	}

	public Filter like(Object value) {
		return new ExpressionCondition(ConditionMode.FUZZY, propertyName, value);
	}
	
	public Filter notlike(Object value) {
		return new ExpressionCondition(ConditionMode.NOT_FUZZY, propertyName, value);
	}

	public Filter lt(Object value) {
		return new ExpressionCondition(ConditionMode.LESS_THEN, propertyName, value);
	}

	public ExpressionProperty ltProperty(String otherPropertyName) {
		return new ExpressionProperty(propertyName, otherPropertyName, ConditionMode.LESS_THEN);
	}

	public Filter ne(Object value) {
		return new ExpressionCondition(ConditionMode.NOT_EMPTY, propertyName, value);
	}

	public ExpressionProperty neProperty(String otherPropertyName) {
		return new ExpressionProperty(propertyName, otherPropertyName, ConditionMode.NOT_EQUAL);
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public ExpressionType getType() {
		return type;
	}

	public void setType(ExpressionType type) {
		this.type = type;
	}

}
