package com.mapfinal.render;


import com.mapfinal.geometry.GeomType;
import com.mapfinal.map.Feature;
import com.mapfinal.render.style.SimpleFillSymbol;
import com.mapfinal.render.style.SimpleLineSymbol;
import com.mapfinal.render.style.SimpleMarkerSymbol;
import com.mapfinal.render.style.Symbol;

public abstract class SimpleFeatureRenderer extends SimpleRenderer {

	public SimpleFeatureRenderer(Symbol symbol) {
		super(symbol);
	}
	
	public SimpleFeatureRenderer(int color, boolean pickMode) {
		super(color, pickMode);
	}
	
	public abstract Symbol eachFeatureSymbol(Feature feature);

	@Override
	public Symbol getSymbol(Feature feature) {
		// TODO Auto-generated method stub
		if(isPickMode()) {
			if(feature==null || feature.getGeometry()==null) return null;
			//
			if(getSymbol()!=null) {
				return getSymbol().getPickSymbol(getPickColor());
			}
			//
			String gtype = feature.getGeometry().getGeometryType();
			GeomType type = GeomType.valueOf(gtype.toUpperCase());
			if(type == GeomType.MULTIPOLYGON || type==GeomType.POLYGON) {
				return SimpleFillSymbol.create(getPickColor(), getPickColor());
			} else if(type == GeomType.MULTILINESTRING || type==GeomType.LINERING
					|| type == GeomType.LINESEGMENT || type==GeomType.LINESTRING) {
				return SimpleLineSymbol.create(getPickColor());
			} else if(type == GeomType.MULTIPOINT || type==GeomType.POINT) {
				return SimpleMarkerSymbol.create(getPickColor());
			}
			return SimpleFillSymbol.create(getPickColor(), getPickColor());
		}
		Symbol symbol = eachFeatureSymbol(feature);
		symbol = symbol == null ? getSymbol() : symbol; 
		return symbol;
	}
}
