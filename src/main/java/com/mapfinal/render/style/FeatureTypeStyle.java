package com.mapfinal.render.style;

import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.mapfinal.MapfinalObject;
import com.mapfinal.converter.JsonStore;
import com.mapfinal.kit.StringKit;

/**
 * The FeatureTypeStyle element specifies the styling that is applied to a single feature type of a layer. 
 * It contains a list of rules which determine the symbology to be applied to each feature of a layer.
 * @author yangyong
 *
 */
public class FeatureTypeStyle implements MapfinalObject, JsonStore {

	private String name;
	private String title;
	private String description;
	private List<StyleRule> rules;
	
	public FeatureTypeStyle() {
		setRules(new ArrayList<StyleRule>());
	}

	@Override
	public void fromJson(JSONObject jsonObject) {
		// TODO Auto-generated method stub
		name = jsonObject.containsKey("name") ? jsonObject.getString("name") : name;
		title = jsonObject.containsKey("title") ? jsonObject.getString("title") : title;
		description = jsonObject.containsKey("description") ? jsonObject.getString("description") : description;
		if(jsonObject.containsKey("rules")) {
			rules.clear();
			JSONArray jsonArray = jsonObject.getJSONArray("rules");
			for (int i = 0; i < jsonArray.size(); i++) {
				StyleRule rule = new StyleRule();
				rule.fromJson(jsonArray.getJSONObject(i));
				rules.add(rule);
			}
		}
	}

	@Override
	public JSONObject toJson() {
		// TODO Auto-generated method stub
		JSONObject json = new JSONObject();
		if(StringKit.notBlank(name)) json.put("name", name);
		if(StringKit.notBlank(title)) json.put("title", title);
		if(StringKit.notBlank(description)) json.put("description", description);
		if(rules.size() > 0) {
			JSONArray jsonArray = new JSONArray();
			for (int i = 0; i < rules.size(); i++) {
				StyleRule rule = rules.get(i);
				jsonArray.add(rule.toJson());
			}
			json.put("rules", jsonArray);
		}
		return json;
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		rules.clear();
	}

	public List<StyleRule> getRules() {
		return rules;
	}

	public void setRules(List<StyleRule> rules) {
		this.rules = rules;
	}
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
