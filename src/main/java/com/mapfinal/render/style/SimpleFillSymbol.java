package com.mapfinal.render.style;

import com.alibaba.fastjson.JSONObject;
import com.mapfinal.Mapfinal;
import com.mapfinal.kit.ColorKit;

public class SimpleFillSymbol extends FillSymbol {

	/**
	 * 绘制方式
	 */
	public enum STYLE {
		/**
		 * 右上方向斜线填充
		 */
		BACKWARD_DIAGONAL,
		/**
		 * 网格填充
		 */
		CROSS,
		/**
		 * 斜向网格填充
		 */
		DIAGONAL_CROSS,
		/**
		 * 左上方向斜线填充
		 */
		FORWARD_DIAGONAL,
		/**
		 * 横线填充
		 */
		HORIZONTAL,
		/**
		 * 未知
		 */
		NULL,
		/**
		 * 颜色填充
		 */
		SOLID,
		/**
		 * 竖线填充
		 */
		VERTICAL
	}

	private int opacity = 255;
	private int color = 0;
	private SimpleFillSymbol.STYLE type = STYLE.SOLID;
	private SimpleLineSymbol outline;
	// A string that defines how the inside of a shape is determined.
	// 路径填充，默认奇数多边形是空洞
	private String fillRule = "evenodd";

	public SimpleFillSymbol(int color) {
		// TODO Auto-generated constructor stub
		this.color = color;
	}

	public SimpleFillSymbol(int color, SimpleFillSymbol.STYLE type) {
		// TODO Auto-generated constructor stub
		this.color = color;
		this.type = type;
	}

	public SimpleFillSymbol(SimpleFillSymbol symbol) {
		// TODO Auto-generated constructor stub
		if (symbol != null) {
			this.opacity = symbol.getOpacity();
			this.color = symbol.getColor();
			this.type = symbol.type;
			this.outline = new SimpleLineSymbol(symbol.outline);
		}
	}

	@Override
	public int getOpacity() {
		// TODO Auto-generated method stub
		return opacity;
	}

	@Override
	public int getColor() {
		// TODO Auto-generated method stub
		return color;
	}

	@Override
	public LineSymbol getOutline() {
		// TODO Auto-generated method stub
		return outline;
	}

	@Override
	public void setOpacity(int opacity) {
		// TODO Auto-generated method stub
		this.opacity = opacity;
	}

	@Override
	public void setColor(int color) {
		// TODO Auto-generated method stub
		this.color = color;
	}

	@Override
	public void setOutline(LineSymbol outline) {
		// TODO Auto-generated method stub
		this.outline = (SimpleLineSymbol) outline;
	}

	public SimpleFillSymbol.STYLE getType() {
		return type;
	}

	public void setType(SimpleFillSymbol.STYLE type) {
		this.type = type;
	}

	@Override
	public void fromJson(JSONObject jsonObject) {
		// TODO Auto-generated method stub
		opacity = jsonObject.getIntValue("opacity");
		if (jsonObject.containsKey("color")) {
			String colorString = jsonObject.getString("color");
			color = ColorKit.parseColor(colorString);
		}
		// color = jsonObject.getIntValue("color");
		type = jsonObject.getObject("type", SimpleFillSymbol.STYLE.class);
		fillRule = jsonObject.containsKey("fillRule") ? jsonObject.getString("fillRule") : fillRule;
		if (jsonObject.containsKey("outline")) {
			SimpleLineSymbol line = SimpleLineSymbol.DEFAULT();
			line.fromJson(jsonObject.getJSONObject("outline"));
			outline = line;
		}
	}

	@Override
	public JSONObject toJson() {
		// TODO Auto-generated method stub
		JSONObject json = new JSONObject();
		json.put("opacity", opacity);
		json.put("color", ColorKit.convertToHexValue(color));
		json.put("type", type);
		json.put("fillRule", fillRule);
		if (outline != null) {
			json.put("outline", outline.toJson());
		}
		return json;
	}

	@Override
	public FillSymbol getPickSymbol(int color) {
		// TODO Auto-generated method stub
		SimpleFillSymbol symbol = new SimpleFillSymbol(this);
		symbol.setColor(color);
		symbol.setOpacity(255);
		if (symbol.getOutline() != null) {
			symbol.getOutline().setColor(color);
			symbol.getOutline().setOpacity(255);
			symbol.getOutline().setWidth(symbol.getOutline().getWidth() + Mapfinal.me().getPickLineZommPixel());
		}
		return symbol;
	}

	public String getFillRule() {
		return fillRule;
	}

	public void setFillRule(String fillRule) {
		this.fillRule = fillRule;
	}
}
