package com.mapfinal.render.style;

import com.alibaba.fastjson.JSONObject;
import com.mapfinal.Mapfinal;

public class SimpleMarkerSymbol extends MarkerSymbol {

	private SimpleFillSymbol fill;
	private MarkerSymbol.STYLE type = STYLE.CIRCLE;

	public SimpleMarkerSymbol(int color) {
		// TODO Auto-generated constructor stub
		this.fill = new SimpleFillSymbol(color);
	}
	
	public SimpleMarkerSymbol(SimpleFillSymbol fill) {
		// TODO Auto-generated constructor stub
		this.fill = fill;
	}
	
	public SimpleMarkerSymbol(SimpleMarkerSymbol symbol) {
		// TODO Auto-generated constructor stub
		super(symbol);
		if(symbol!=null) {
			this.fill = new SimpleFillSymbol(symbol.getFill());
			this.type = symbol.getType();
		}
	}
	
	public SimpleMarkerSymbol(SimpleFillSymbol fill, float width, float height) {
		// TODO Auto-generated constructor stub
		this.fill = fill;
		setWidth(width);
		setHeight(height);
	}

	@Override
	public void fromJson(JSONObject jsonObject) {
		// TODO Auto-generated method stub
		super.fromJson(jsonObject);
		type = jsonObject.getObject("type", MarkerSymbol.STYLE.class);
		if(jsonObject.containsKey("fill")) {
			SimpleFillSymbol fs = SimpleFillSymbol.DEFAULT();
			fs.fromJson(jsonObject.getJSONObject("fill"));
			fill = fs;
		}
	}

	@Override
	public JSONObject toJson() {
		// TODO Auto-generated method stub
		JSONObject json = super.toJson();
		json.put("type", type);
		if(fill!=null) {
			json.put("fill", fill.toJson());
		}
		return json;
	}

	public SimpleFillSymbol getFill() {
		return fill;
	}

	public void setFill(SimpleFillSymbol fill) {
		this.fill = fill;
	}

	public MarkerSymbol.STYLE getType() {
		return type;
	}

	public void setType(MarkerSymbol.STYLE style) {
		this.type = style;
	}

	@Override
	public MarkerSymbol getPickSymbol(int color) {
		// TODO Auto-generated method stub
		SimpleMarkerSymbol symbol = new SimpleMarkerSymbol(this);
		symbol.getFill().setColor(color);
		symbol.setWidth(this.getWidth() + Mapfinal.me().getPickPointZommPixel());
		symbol.setHeight(this.getHeight() + Mapfinal.me().getPickPointZommPixel());
		if(symbol.getFill().getOutline()!=null) {
			symbol.getFill().getOutline().setColor(color);
		}
		return symbol;
	}
}
