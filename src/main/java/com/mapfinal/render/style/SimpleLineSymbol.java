package com.mapfinal.render.style;

import com.alibaba.fastjson.JSONObject;
import com.mapfinal.Mapfinal;
import com.mapfinal.kit.ColorKit;

public class SimpleLineSymbol extends LineSymbol {

	public enum STYLE {
		/**
		 * 虚线
		 */
		DASH,
		/**
		 * 点划线
		 */
		DASHDOT,
		/**
		 * 双点划线
		 */
		DASHDOTDOT,
		/**
		 * 点线
		 */
		DOT,
		/**
		 * 未知类型
		 */
		NULL,
		/**
		 * 实线
		 */
		SOLID
	}

	private int opacity = 255;
	private int color = 0;
	private SimpleLineSymbol.STYLE type = STYLE.SOLID;
	private SimpleLineSymbol outline;
	private float width = 1.0f;
	// A string that defines shape to be used at the end of the stroke.
	private String lineCap = "round";
	// A string that defines shape to be used at the corners of the stroke.
	private String lineJoin = "round";
	// A string that defines the stroke dash pattern. Doesn"t work on
	// Canvas-powered layers in some old browsers.
	private String dashArray = null;
	// A string that defines the distance into the dash pattern to start the
	// dash. Doesn"t work on Canvas-powered layers in some old browsers.
	private String dashOffset = null;

	public SimpleLineSymbol(int color) {
		this.color = color;
	}

	public SimpleLineSymbol(int color, float width) {
		this.color = color;
		this.width = width;
	}

	public SimpleLineSymbol(int color, float width, SimpleLineSymbol.STYLE type) {
		this.color = color;
		this.width = width;
		this.type = type;
	}

	public SimpleLineSymbol(SimpleLineSymbol symbol) {
		if (symbol != null) {
			this.color = symbol.getColor();
			this.width = symbol.getWidth();
			this.type = symbol.getType();
			this.outline = new SimpleLineSymbol(symbol.getOutline());
			this.opacity = symbol.opacity;
		}
	}

	@Override
	public int getOpacity() {
		// TODO Auto-generated method stub
		return opacity;
	}

	@Override
	public int getColor() {
		// TODO Auto-generated method stub
		return color;
	}

	@Override
	public void setOpacity(int opacity) {
		// TODO Auto-generated method stub
		this.opacity = opacity;
	}

	@Override
	public void setColor(int color) {
		// TODO Auto-generated method stub
		this.color = color;
	}

	@Override
	public float getWidth() {
		// TODO Auto-generated method stub
		return width;
	}

	@Override
	public void setWidth(float width) {
		// TODO Auto-generated method stub
		this.width = width;
	}

	public SimpleLineSymbol getOutline() {
		return outline;
	}

	/**
	 * 获取线样式的类型
	 * 
	 * @return
	 */
	public SimpleLineSymbol.STYLE getType() {
		return type;
	}

	public void setOutline(SimpleLineSymbol outline) {
		this.outline = outline;
	}

	/**
	 * 设置线样式的类型
	 * 
	 * @param type
	 */
	public void setType(SimpleLineSymbol.STYLE type) {
		this.type = type;
	}

	@Override
	public void fromJson(JSONObject jsonObject) {
		// TODO Auto-generated method stub
		opacity = jsonObject.getIntValue("opacity");
		if (jsonObject.containsKey("color")) {
			String colorString = jsonObject.getString("color");
			color = ColorKit.parseColor(colorString);
		}
		// color = jsonObject.getIntValue("color");
		type = jsonObject.getObject("type", SimpleLineSymbol.STYLE.class);
		width = jsonObject.getFloatValue("width");
		lineCap = jsonObject.containsKey("lineCap") ? jsonObject.getString("lineCap") : lineCap;
		lineJoin = jsonObject.containsKey("lineJoin") ? jsonObject.getString("lineJoin") : lineJoin;
		dashArray = jsonObject.containsKey("dashArray") ? jsonObject.getString("dashArray") : dashArray;
		dashOffset = jsonObject.containsKey("dashOffset") ? jsonObject.getString("dashOffset") : dashOffset;
		if (jsonObject.containsKey("outline")) {
			SimpleLineSymbol line = SimpleLineSymbol.DEFAULT();
			line.fromJson(jsonObject.getJSONObject("outline"));
			outline = line;
		}
	}

	@Override
	public JSONObject toJson() {
		// TODO Auto-generated method stub
		JSONObject json = new JSONObject();
		json.put("opacity", opacity);
		json.put("color", ColorKit.convertToHexValue(color));
		json.put("type", type);
		json.put("width", width);
		json.put("lineCap", lineCap);
		json.put("lineJoin", lineJoin);
		json.put("dashArray", dashArray);
		json.put("dashOffset", dashOffset);
		if (outline != null) {
			json.put("outline", outline.toJson());
		}
		return json;
	}

	@Override
	public LineSymbol getPickSymbol(int color) {
		// TODO Auto-generated method stub
		SimpleLineSymbol symbol = new SimpleLineSymbol(this);
		symbol.setColor(color);
		symbol.setOpacity(255);
		symbol.setWidth(this.getWidth() + Mapfinal.me().getPickLineZommPixel());
		return symbol;
	}

	public String getLineCap() {
		return lineCap;
	}

	public void setLineCap(String lineCap) {
		this.lineCap = lineCap;
	}

	public String getLineJoin() {
		return lineJoin;
	}

	public void setLineJoin(String lineJoin) {
		this.lineJoin = lineJoin;
	}

	public String getDashArray() {
		return dashArray;
	}

	public void setDashArray(String dashArray) {
		this.dashArray = dashArray;
	}

	public String getDashOffset() {
		return dashOffset;
	}

	public void setDashOffset(String dashOffset) {
		this.dashOffset = dashOffset;
	}

}
