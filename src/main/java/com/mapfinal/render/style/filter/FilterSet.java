package com.mapfinal.render.style.filter;

import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.mapfinal.converter.JsonStore;

/**
 * https://docs.geoserver.org/latest/en/user/styling/sld/reference/filters.html#comparison-operators
 * @author yangyong
 *
 */
public class FilterSet implements JsonStore {

	private FilterProperty property;
	private List<FilterGroup> filterGroup;

	public FilterSet() {
		// TODO Auto-generated constructor stub
	}

	public void add(FilterProperty property) {
		this.property = property;
	}

	@Override
	public void fromJson(JSONObject jsonObject) {
		// TODO Auto-generated method stub
		if (jsonObject.containsKey("and")) {
			JSONArray jsonArray = jsonObject.getJSONArray("and");
			for (int i = 0; i < jsonArray.size(); i++) {
			}

		}
		if (jsonObject.containsKey("or")) {
			JSONArray jsonArray = jsonObject.getJSONArray("or");
			for (int i = 0; i < jsonArray.size(); i++) {
			}

		}
	}

	@Override
	public JSONObject toJson() {
		// TODO Auto-generated method stub
		JSONObject json = new JSONObject();
		//json.put("", value)
		
		return json;
	}

	public FilterProperty getProperty() {
		return property;
	}

	public void setProperty(FilterProperty property) {
		this.property = property;
	}

	public List<FilterGroup> getFilterGroup() {
		return filterGroup;
	}

	public void setFilterGroup(List<FilterGroup> filterGroup) {
		this.filterGroup = filterGroup;
	}

}