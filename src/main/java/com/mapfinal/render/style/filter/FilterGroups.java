package com.mapfinal.render.style.filter;

import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.mapfinal.converter.JsonStore;

public class FilterGroups implements JsonStore {

	// and,or,not
	private String type = "and";
	private List<FilterProperty> properties;
	private FilterGroups group;

	public FilterGroups(String type) {
		// TODO Auto-generated constructor stub
		this.type = type;
		properties = new ArrayList<FilterProperty>();
	}

	public void add(FilterProperty property) {
		properties = new ArrayList<FilterProperty>();
		properties.add(property);
	}

	@Override
	public void fromJson(JSONObject jsonObject) {
		// TODO Auto-generated method stub
		if(!jsonObject.containsKey(type)) return;
		
		JSONArray jsonArray = jsonObject.getJSONArray(type);
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject jsonItem = jsonArray.getJSONObject(i);
			if (jsonObject.containsKey("and")) {
				group = new FilterGroups("and");
				group.fromJson(jsonItem);
			} else if (jsonObject.containsKey("or")) {
				group = new FilterGroups("or");
				group.fromJson(jsonItem);
			} else {
				FilterProperty prpt = new FilterProperty();
				prpt.fromJson(jsonItem);
				properties.add(prpt);
			}
		}
	}

	@Override
	public JSONObject toJson() {
		// TODO Auto-generated method stub
		JSONObject json = new JSONObject();
		if(properties!=null && properties.size() > 0) {
			JSONArray jsonArray = new JSONArray();
			for (int i = 0; i < properties.size(); i++) {
				JSONObject obj = properties.get(i).toJson();
				jsonArray.add(obj);
			}
			if(group!=null) {
				JSONObject groupJson = group.toJson();
				jsonArray.add(groupJson);
			}
			json.put(type, jsonArray);
		}
		return json;
	}

	public List<FilterProperty> getProperties() {
		return properties;
	}

	public void setProperties(List<FilterProperty> properties) {
		this.properties = properties;
	}

	public FilterGroups getGroup() {
		return group;
	}

	public void setGroup(FilterGroups group) {
		this.group = group;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}