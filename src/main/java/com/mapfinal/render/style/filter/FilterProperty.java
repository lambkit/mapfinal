package com.mapfinal.render.style.filter;

import java.util.List;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.mapfinal.converter.JsonStore;
import com.mapfinal.dispatcher.query.ConditionMode;

public class FilterProperty implements JsonStore {
	// 名称
	private String name;
	// 条件类型
	private ConditionMode type;
	// 条件值
	private Object value;
	private Object secondValue;
	private boolean noValue;
	private boolean singleValue;
	private boolean betweenValue;
	private boolean listValue;
	
	public FilterProperty() {
		// TODO Auto-generated constructor stub
	}

	public FilterProperty(ConditionMode type, String name) {
		this.type = type;
		this.setName(name);
		// this.typeHandler = null;
		this.noValue = true;
	}

	public FilterProperty(ConditionMode type, String name, Object value) {// String typeHandler, String valueType
		this.type = type;
		this.value = value;
		this.setName(name);
		if (value instanceof List<?>) {
			this.listValue = true;
		} else {
			this.singleValue = true;
		}
		// this.valueType = valueType;
	}

	@Override
	public void fromJson(JSONObject jsonObject) {
		// TODO Auto-generated method stub
		FilterProperty property = JSON.toJavaObject(jsonObject, FilterProperty.class);
		this.name = property.getName();
		this.type = property.getType();
		this.value = property.getValue();
		this.secondValue = property.getSecondValue();
		this.noValue = property.isNoValue();
		this.singleValue = property.isSingleValue();
		this.betweenValue = property.isBetweenValue();
		this.listValue = property.isListValue();
	}

	@Override
	public JSONObject toJson() {
		// TODO Auto-generated method stub
		return (JSONObject) JSON.toJSON(this);
	}

	public ConditionMode getType() {
		return type;
	}

	public void setType(ConditionMode type) {
		this.type = type;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	public Object getSecondValue() {
		return secondValue;
	}

	public void setSecondValue(Object secondValue) {
		this.secondValue = secondValue;
	}

	public boolean isNoValue() {
		return noValue;
	}

	public void setNoValue(boolean noValue) {
		this.noValue = noValue;
	}

	public boolean isSingleValue() {
		return singleValue;
	}

	public void setSingleValue(boolean singleValue) {
		this.singleValue = singleValue;
	}

	public boolean isBetweenValue() {
		return betweenValue;
	}

	public void setBetweenValue(boolean betweenValue) {
		this.betweenValue = betweenValue;
	}

	public boolean isListValue() {
		return listValue;
	}

	public void setListValue(boolean listValue) {
		this.listValue = listValue;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
