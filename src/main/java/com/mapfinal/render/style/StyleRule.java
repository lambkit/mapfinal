package com.mapfinal.render.style;

import com.alibaba.fastjson.JSONObject;
import com.mapfinal.MapfinalObject;
import com.mapfinal.converter.JsonStore;

public class StyleRule implements MapfinalObject, JsonStore {
	// 过滤条件
	private StyleFilter filter;
	// 渲染symbol
	private Symbol symbol;

	@Override
	public void fromJson(JSONObject jsonObject) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public JSONObject toJson() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	public StyleFilter getFilter() {
		return filter;
	}

	public void setFilter(StyleFilter filter) {
		this.filter = filter;
	}

	public Symbol getSymbol() {
		return symbol;
	}

	public void setSymbol(Symbol symbol) {
		this.symbol = symbol;
	}

}
