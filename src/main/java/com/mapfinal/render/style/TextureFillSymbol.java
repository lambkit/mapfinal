package com.mapfinal.render.style;

import com.alibaba.fastjson.JSONObject;
import com.mapfinal.kit.ColorKit;
import com.mapfinal.resource.image.Image;

public class TextureFillSymbol extends FillSymbol {

	private int opacity = 255;
	private int color = 0;
	private LineSymbol outline;
	private Image image;
	private int textureId;//for 3D
	private boolean picked = false;
	// 路径填充，默认奇数多边形是空洞
		private String fillRule = "evenodd";

	public TextureFillSymbol(int color) {
		// TODO Auto-generated constructor stub
		this.color = color;
	}

	public TextureFillSymbol(TextureFillSymbol symbol) {
		// TODO Auto-generated constructor stub
		this.opacity = symbol.getOpacity();
		this.color = symbol.getColor();
		this.outline = symbol.outline;
	}

	@Override
	public int getOpacity() {
		// TODO Auto-generated method stub
		return opacity;
	}

	@Override
	public int getColor() {
		// TODO Auto-generated method stub
		return color;
	}

	@Override
	public LineSymbol getOutline() {
		// TODO Auto-generated method stub
		return outline;
	}

	@Override
	public void setOpacity(int opacity) {
		// TODO Auto-generated method stub
		this.opacity = opacity;
	}

	@Override
	public void setColor(int color) {
		// TODO Auto-generated method stub
		this.color = color;
	}

	@Override
	public void setOutline(LineSymbol outline) {
		// TODO Auto-generated method stub
		this.outline = outline;
	}

	@Override
	public void fromJson(JSONObject jsonObject) {
		// TODO Auto-generated method stub
		opacity = jsonObject.getIntValue("opacity");
		if(jsonObject.containsKey("color")) {
			String colorString = jsonObject.getString("color");
			color = ColorKit.parseColor(colorString);
		}
		//color = jsonObject.getIntValue("color");
		textureId = jsonObject.getIntValue("textureId");
		picked = jsonObject.getBooleanValue("picked");
		fillRule = jsonObject.containsKey("fillRule") ? jsonObject.getString("fillRule") : fillRule;
		if(jsonObject.containsKey("outline")) {
			SimpleLineSymbol line = SimpleLineSymbol.DEFAULT();
			line.fromJson(jsonObject.getJSONObject("outline"));
			outline = line;
		}
		if(jsonObject.containsKey("image")) {
			image = new Image(null, null, null);
			image.fromJson(jsonObject.getJSONObject("image"));
		}
	}

	@Override
	public JSONObject toJson() {
		// TODO Auto-generated method stub
		JSONObject json = new JSONObject();
		json.put("opacity", opacity);
		json.put("color", ColorKit.convertToHexValue(color));
		json.put("textureId", textureId);
		json.put("picked", picked);
		json.put("fillRule", fillRule);
		if(outline!=null) {
			json.put("outline", outline.toJson());
		}
		if(image!=null) {
			json.put("image", image.toJson());
		}
		return json;
	}

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}

	public int getTextureId() {
		return textureId;
	}

	public void setTextureId(int textureId) {
		this.textureId = textureId;
	}

	@Override
	public FillSymbol getPickSymbol(int color) {
		// TODO Auto-generated method stub
		TextureFillSymbol symbol = new TextureFillSymbol(this);
		symbol.setColor(color);
		symbol.setOpacity(255);
		symbol.setPicked(true);
		if(symbol.getOutline()!=null) {
			symbol.getOutline().setColor(color);
			symbol.getOutline().setOpacity(255);
		}
		return symbol;
	}

	public boolean isPicked() {
		return picked;
	}

	public void setPicked(boolean picked) {
		this.picked = picked;
	}

	public String getFillRule() {
		return fillRule;
	}

	public void setFillRule(String fillRule) {
		this.fillRule = fillRule;
	}

}
