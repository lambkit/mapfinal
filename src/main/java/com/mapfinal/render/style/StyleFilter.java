package com.mapfinal.render.style;

import com.alibaba.fastjson.JSONObject;
import com.mapfinal.MapfinalObject;
import com.mapfinal.converter.JsonStore;
import com.mapfinal.dispatcher.query.FilterSet;

public class StyleFilter implements MapfinalObject, JsonStore {
	
	private FilterSet filter;

	@Override
	public void fromJson(JSONObject jsonObject) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public JSONObject toJson() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	public FilterSet getFilter() {
		return filter;
	}

	public void setFilter(FilterSet filter) {
		this.filter = filter;
	}

}
