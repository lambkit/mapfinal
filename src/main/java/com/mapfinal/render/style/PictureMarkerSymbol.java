package com.mapfinal.render.style;

import com.alibaba.fastjson.JSONObject;
import com.mapfinal.resource.image.Image;

public class PictureMarkerSymbol extends MarkerSymbol {

	private Image image;
	private MarkerSymbol.STYLE type = STYLE.ICON;

	public PictureMarkerSymbol(Image image) {
		// TODO Auto-generated constructor stub
		this.image = image;
	}

	public MarkerSymbol.STYLE getType() {
		return type;
	}

	public void setType(MarkerSymbol.STYLE type) {
		this.type = type;
	}

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}

	@Override
	public void fromJson(JSONObject jsonObject) {
		// TODO Auto-generated method stub
		super.fromJson(jsonObject);
		type = jsonObject.getObject("type", MarkerSymbol.STYLE.class);
		if (jsonObject.containsKey("image")) {
			image = new Image(null, null, null);
			image.fromJson(jsonObject.getJSONObject("image"));
		}
	}

	@Override
	public JSONObject toJson() {
		// TODO Auto-generated method stub
		JSONObject json = super.toJson();
		json.put("type", type);
		if (image != null) {
			json.put("image", image.toJson());
		}
		return json;
	}

	@Override
	public MarkerSymbol getPickSymbol(int color) {
		// TODO Auto-generated method stub
		return new SimpleMarkerSymbol(color);
	}
}
