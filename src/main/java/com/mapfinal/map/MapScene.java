package com.mapfinal.map;

import com.mapfinal.event.EventKit;
import com.mapfinal.event.EventManager;
import com.mapfinal.geometry.Latlng;
import com.mapfinal.processor.simplify.GeoSimplifier;
import com.mapfinal.render.RenderCompress;
import com.mapfinal.render.SceneGraph;
import com.mapfinal.render.SceneRedrawListener;
import com.mapfinal.render.SimpleRenderCompress;

/**
 * 地图场景
 * 
 * @author yangyong
 *
 */
public abstract class MapScene extends SceneGraph {

	private MapView view = new MapView();
	private RenderCompress renderCompress;
	
	public MapScene() {
		this.addNode(view);
		EventManager.me().registerListener(SceneRedrawListener.class);
	}
	
	/**
	 * 获取不同渲染级别下，图形渲染时如何较少渲染点数量
	 * @param context
	 * @param nPoints
	 * @return
	 */
	public RenderCompress getRenderCompress(GeoSimplifier.Type type) {
		if(renderCompress==null) {
			renderCompress = new SimpleRenderCompress(type);
		}
		return renderCompress;
	}

	public void addLayer(Layer layer) {
		view.add(layer);
	}

	public void setCenter(Latlng center) {
		view.setCenter(center);
	}

	public void setZoom(float zoom) {
		view.setZoom(zoom);
	}

	public void resize(int width, int height) {
		view.resize(width, height);
	}

	public MapView getMap() {
		return view;
	}

	public MapView map() {
		return view;
	}

	public MapView view() {
		return view;
	}

	public MapContext context() {
		return view.getContext();
	}

	public void redraw() {
		EventKit.sendEvent("redraw");
	}

	public void setRenderCompress(RenderCompress renderCompress) {
		this.renderCompress = renderCompress;
	}

}
