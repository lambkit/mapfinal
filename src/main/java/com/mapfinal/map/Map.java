package com.mapfinal.map;

import com.mapfinal.converter.CRS;
import com.mapfinal.converter.ConverterManager;
import com.mapfinal.converter.SpatialReference;
import com.mapfinal.converter.scene.SceneCRS;
import com.mapfinal.kit.StringKit;

public class Map {
	
	private int width;
	private int height;
	private MapContext context;

	public Map(int width, int height, SceneCRS sceneCRS) {
		init(width, height, sceneCRS);
	}
	
	public Map(int width, int height, SpatialReference spatialReference) {
		SceneCRS sceneCRS = null;
		if(spatialReference!=null) {
			CRS crs = ConverterManager.me().getCRS(spatialReference.getName());
			sceneCRS = ConverterManager.me().getFactory().toSceneCRS(crs);
		}
		init(width, height, sceneCRS);
	}
	
	public Map(int width, int height, CRS crs) {
		init(width, height, ConverterManager.me().getFactory().toSceneCRS(crs));
	}
	
	public Map(int width, int height, String proj4) {
		SceneCRS sceneCRS = null;
		if(StringKit.isNotBlank(proj4)) {
			CRS crs = new CRS(null, new String[]{proj4});
			sceneCRS = ConverterManager.me().getFactory().toSceneCRS(crs);
		}
		init(width, height, sceneCRS);
	}
	
	private void init(int width, int height, SceneCRS sceneCRS) {
		this.width = width;
		this.height = height;
		context = new MapContext(sceneCRS);
		context.setMainThread(true);
	}
	
	public void resize(int width, int height) {
		this.width = width;
		this.height = height;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public MapContext getContext() {
		return context;
	}

	public void setContext(MapContext context) {
		this.context = context;
	}
	
}
